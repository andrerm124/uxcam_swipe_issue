import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import DemoScreen1 from './screens/DemoScreen1';
import DemoScreen2 from './screens/DemoScreen2';

const MainStack = createStackNavigator({
  Demo1: { screen: DemoScreen1 },
  Demo2: { screen: DemoScreen2 },
});

const Router = createAppContainer(MainStack);

export default Router;
