import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { GestureDemo } from '../GestureDemo';

class DemoScreen2 extends Component {
  render() {
    const {} = this.props;
    const {} = styles;

    return (
      <View style={{ flex: 1 }}>
        <GestureDemo screenName={'DemoScreen 2'} />
      </View>
    );
  }
}

const styles = {};

export default DemoScreen2;
