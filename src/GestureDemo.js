import React, { useState } from 'react';
import { PanResponder, Text, View, StyleSheet } from 'react-native';

const GestureDemo = ({ screenName = 'DemoScreen' }) => {
  const [gestureCount, setGestureCount] = useState(0);

  const handleGesture = () => {
    console.log('Gesture capture');
    setGestureCount(Number(gestureCount) + 1);
  };

  const panResponder = PanResponder.create({
    onMoveShouldSetPanResponder: handleGesture,
  });

  return (
    <View style={styles.containerStyle} {...panResponder.panHandlers}>
      <Text style={styles.textStyle}>{screenName}</Text>
      <Text style={styles.textStyle}>Gesture Count</Text>
      <Text style={styles.textStyle}>{gestureCount}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle: { fontSize: 25, fontWeight: '800' },
});

export { GestureDemo };
