/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { SafeAreaView } from 'react-native';

import { GestureDemo } from './src/GestureDemo';
import Router from './src/Router';

const App: () => React$Node = () => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Router />
    </SafeAreaView>
  );
};

export default App;
